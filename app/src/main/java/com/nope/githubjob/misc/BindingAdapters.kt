package com.nope.githubjob.misc

import android.arch.paging.PagedList
import android.arch.paging.PagedListAdapter
import android.databinding.BindingAdapter
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.nope.githubjob.R

@BindingAdapter("visibleIf")
fun View.visibleIf(condition: Boolean) {
    visibility = if (condition) View.VISIBLE else View.GONE
}

@BindingAdapter("textHtml")
fun TextView.textHtml(htmlText: String?) {
    text = if (htmlText != null) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(htmlText, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(htmlText)
        }
    } else {
        ""
    }
}

@BindingAdapter("imageUrl")
fun ImageView.imageUrl(imageUrl: String?) {
    GlideApp.with(this)
        .load(imageUrl)
        .placeholder(R.drawable.ic_photo_black_24dp)
        .into(this)
}

@BindingAdapter("adapter")
fun <T, VH : RecyclerView.ViewHolder> setAdapter(
    recyclerView: RecyclerView,
    adapter: PagedListAdapter<T, VH>
) {
    recyclerView.adapter = adapter
}

@BindingAdapter("pagedItems")
fun <T, VH : RecyclerView.ViewHolder> setData(recyclerView: RecyclerView, items: PagedList<T>?) {
    when (recyclerView.adapter) {
        is PagedListAdapter<*, *> -> (recyclerView.adapter as PagedListAdapter<T, VH>).submitList(items)
    }
}
