package com.nope.githubjob.misc

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by IntelliJ IDEA.
 * Author: aleksander.kiryuhin@e-legion.com
 * Date: 20.08.18
 */

@GlideModule
class MyAppGlideModule : AppGlideModule()

