package com.nope.githubjob.misc

import com.nope.githubjob.model.local.PositionDescription
import com.nope.githubjob.model.local.PositionPreview
import com.nope.githubjob.model.remote.Position

/**
 * Created by IntelliJ IDEA.
 * Author: aleksander.kiryuhin@e-legion.com
 * Date: 17.08.18
 */

fun Position.toPreview() = PositionPreview(id, title, location, company_logo)

fun List<Position>.toPreview() = map { it.toPreview() }

fun Position.toDescription() = PositionDescription(description)
