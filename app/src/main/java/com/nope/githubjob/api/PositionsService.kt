package com.nope.githubjob.api

import com.nope.githubjob.model.remote.Position
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PositionsService {

    @GET("positions.json")
    fun getPositions(@Query("search") search: String, @Query("page") page: Int): Call<List<Position>>

    @GET("positions/{positionId}.json")
    fun getPosition(@Path("positionId") positionId: String): Call<Position>
}
