package com.nope.githubjob.ui.search

import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import com.nope.githubjob.R
import com.nope.githubjob.databinding.ListItemPositionBinding
import com.nope.githubjob.model.local.PositionPreview
import com.nope.githubjob.ui.common.DataBoundListAdapter

class PositionsAdapter(
    private val itemClickCallback: (PositionPreview) -> Unit
) : DataBoundListAdapter<PositionPreview, ListItemPositionBinding>(DiffCallback) {

    override fun createBinding(parent: ViewGroup): ListItemPositionBinding {
        val binding: ListItemPositionBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_position,
            parent,
            false
        )
        return binding.apply {
            root.setOnClickListener {
                position?.let { p ->
                    itemClickCallback(p)
                }
            }
        }
    }

    override fun bind(binding: ListItemPositionBinding, item: PositionPreview?) {
        binding.position = item
    }

    private companion object DiffCallback : DiffUtil.ItemCallback<PositionPreview>() {

        override fun areItemsTheSame(oldItem: PositionPreview?, newItem: PositionPreview?) = oldItem?.id == newItem?.id

        override fun areContentsTheSame(oldItem: PositionPreview?, newItem: PositionPreview?) = oldItem == newItem
    }
}
