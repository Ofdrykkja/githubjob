package com.nope.githubjob.ui.common

import android.content.Context
import com.nope.githubjob.ui.details.Args
import com.nope.githubjob.ui.details.DetailsFragment
import com.nope.githubjob.ui.main.MainActivity
import com.nope.githubjob.ui.search.SearchFragment
import ru.terrakok.cicerone.android.SupportAppNavigator
import javax.inject.Inject

class MainNavigator @Inject internal constructor(
    activity: MainActivity
) : SupportAppNavigator(activity, android.R.id.content) {

    override fun createActivityIntent(context: Context, screenKey: String, data: Any) = null

    override fun createFragment(screenKey: String, data: Any) = when (screenKey) {
        Screens.SEARCH -> SearchFragment()
        Screens.DETAILS -> DetailsFragment.newInstance(data as Args)
        else -> throw IllegalArgumentException("Unknown screen key: $screenKey")
    }
}
