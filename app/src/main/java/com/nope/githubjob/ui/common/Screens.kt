package com.nope.githubjob.ui.common

object Screens {
    const val SEARCH = "screen_search"
    const val DETAILS = "screen_details"
}
