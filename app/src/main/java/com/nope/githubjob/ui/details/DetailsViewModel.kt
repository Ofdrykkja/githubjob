package com.nope.githubjob.ui.details

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations.map
import android.arch.lifecycle.ViewModel
import com.nope.githubjob.repository.LoadingState
import com.nope.githubjob.repository.PositionsRepository
import com.nope.githubjob.ui.common.LoadingStateSwitcher
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class DetailsViewModel @Inject constructor(
    private val router: Router,
    repository: PositionsRepository
) : ViewModel(), DetailsVm {

    private val positionId = MutableLiveData<String>()

    override val loadingStateSwitcher = LoadingStateSwitcher(map(positionId) { repository.getPosition(it) }) {
        if (it == LoadingState.ERROR) {
            router.showSystemMessage("loading error")
        }
    }

    override fun loadDetails(positionId: String) {
        this.positionId.value = positionId
    }

    override fun close() {
        router.exit()
    }
}