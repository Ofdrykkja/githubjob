package com.nope.githubjob.ui.details

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.nope.githubjob.R
import com.nope.githubjob.databinding.FragmentDetailsBinding
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_details.*
import javax.inject.Inject

class DetailsFragment : DaggerFragment() {

    @Inject
    lateinit var viewModel: DetailsVm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        val companyId = arguments?.getString(BUNDLE_COMPANY_ID)
        if (companyId != null) {
            viewModel.loadDetails(companyId)
        } else {
            throw IllegalStateException("fragment can't be created without companyId argument")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentDetailsBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_details,
            container,
            false
        )
        return binding.also {
            it.viewModel = viewModel
            it.executePendingBindings()
            it.setLifecycleOwner(this)
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).let {
            it.setSupportActionBar(toolbar)
            it.supportActionBar?.apply {
                setDisplayHomeAsUpEnabled(true)
                setDisplayShowHomeEnabled(true)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                viewModel.close()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {

        private const val BUNDLE_COMPANY_ID = "bundle_company_id"

        fun newInstance(args: Args) = DetailsFragment().apply {
            arguments = Bundle(1).apply {
                putString(BUNDLE_COMPANY_ID, args.companyId)
            }
        }
    }
}