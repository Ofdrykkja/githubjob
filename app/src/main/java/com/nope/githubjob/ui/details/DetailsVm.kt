package com.nope.githubjob.ui.details

import com.nope.githubjob.model.local.PositionDescription
import com.nope.githubjob.ui.common.LoadingStateSwitcher

interface DetailsVm {

    val loadingStateSwitcher: LoadingStateSwitcher<PositionDescription>

    fun loadDetails(positionId: String)

    fun close()
}