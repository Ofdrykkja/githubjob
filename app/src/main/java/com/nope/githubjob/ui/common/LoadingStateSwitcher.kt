package com.nope.githubjob.ui.common

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Transformations.map
import android.arch.lifecycle.Transformations.switchMap
import com.nope.githubjob.repository.DataLoader
import com.nope.githubjob.repository.LoadingState

class LoadingStateSwitcher<T>(
    dataLoader: LiveData<DataLoader<LiveData<T>>>,
    private val loadingStateCallback: (LoadingState) -> Unit
) {

    private val isEmpty: LiveData<Boolean> = switchMap(dataLoader) { it.isEmpty }

    private val loadingState: LiveData<LoadingState> = switchMap(dataLoader) { it.loadingState }

    val showProgress: LiveData<Boolean> = map(loadingState) {
        loadingStateCallback(it)
        it == LoadingState.LOADING
    }

    val data: LiveData<T> = switchMap(dataLoader) { it.data }

    val showEmpty = MediatorLiveData<Boolean>()

    val showData = MediatorLiveData<Boolean>()

    init {
        showEmpty.addSource(isEmpty) {
            showEmpty.value = it
        }
        showEmpty.addSource(loadingState) {
            if (it == LoadingState.LOADING) {
                showEmpty.value = false
            }
        }

        showData.addSource(isEmpty) {
            showData.value = it?.not()
        }
        showData.addSource(loadingState) {
            if (it == LoadingState.LOADING) {
                showData.value = false
            }
        }
    }
}