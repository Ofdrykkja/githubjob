package com.nope.githubjob.ui.search

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations.map
import android.arch.lifecycle.ViewModel
import com.nope.githubjob.model.local.PositionPreview
import com.nope.githubjob.repository.LoadingState
import com.nope.githubjob.repository.PositionsRepository
import com.nope.githubjob.ui.common.LoadingStateSwitcher
import com.nope.githubjob.ui.common.Screens
import com.nope.githubjob.ui.details.Args
import ru.terrakok.cicerone.Router

class SearchViewModel(
    repository: PositionsRepository,
    private val router: Router
) : ViewModel(), SearchVm {

    private val init = MutableLiveData<Boolean>()

    private val query = MutableLiveData<String?>()

    override val loadingStateSwitcher = LoadingStateSwitcher(map(query) { repository.getPositions(it) }) {
        if (it == LoadingState.ERROR) {
            router.showSystemMessage("loading error")
        }
    }

    init {
        loadingStateSwitcher.showEmpty.addSource(init) { loadingStateSwitcher.showEmpty.value = it }
        loadingStateSwitcher.showData.addSource(init) { loadingStateSwitcher.showData.value = it?.not() }
        init.value = true
    }

    override fun search(query: String?) {
        this.query.value = query
    }

    override fun onPositionItemClick(position: PositionPreview) {
        router.navigateTo(Screens.DETAILS, Args(position.id))
    }
}