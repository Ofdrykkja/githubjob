package com.nope.githubjob.ui.search

import android.arch.paging.PagedList
import com.nope.githubjob.model.local.PositionPreview
import com.nope.githubjob.ui.common.LoadingStateSwitcher

interface SearchVm {

//    val positions: LiveData<PagedList<PositionPreview>>

    val loadingStateSwitcher: LoadingStateSwitcher<PagedList<PositionPreview>>

    fun search(query: String?)

    fun onPositionItemClick(position: PositionPreview)
}