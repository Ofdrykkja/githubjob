package com.nope.githubjob.di

import com.nope.githubjob.misc.getViewModel
import com.nope.githubjob.repository.PositionsRepository
import com.nope.githubjob.ui.details.DetailsFragment
import com.nope.githubjob.ui.details.DetailsViewModel
import com.nope.githubjob.ui.details.DetailsVm
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
object DetailsScreenModule {

    @JvmStatic
    @Provides
    @FragmentScope
    fun firstScreenVm(
        repository: PositionsRepository,
        router: Router,
        fragment: DetailsFragment
    ): DetailsVm = fragment.getViewModel { DetailsViewModel(router, repository) }
}