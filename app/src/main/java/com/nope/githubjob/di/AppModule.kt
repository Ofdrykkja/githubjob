package com.nope.githubjob.di

import android.arch.paging.PagedList
import com.nope.githubjob.ui.main.MainActivity
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import javax.inject.Singleton

@Module(includes = [DataModule::class, NavigationModule::class])
abstract class AppModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun mainActivity(): MainActivity

    @Module
    companion object {

        @JvmStatic
        @Provides
        @Singleton
        fun pagedListConfig(): PagedList.Config = PagedList.Config.Builder().setPageSize(20).build()
    }
}