package com.nope.githubjob.di

import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import javax.inject.Singleton

@Module
object NavigationModule {

    @JvmStatic
    private val CICERONE = Cicerone.create()

    @JvmStatic
    @Provides
    @Singleton
    internal fun navigatorHolder() = CICERONE.navigatorHolder

    @JvmStatic
    @Provides
    @Singleton
    internal fun router() = CICERONE.router
}