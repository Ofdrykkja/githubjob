package com.nope.githubjob.di

import com.nope.githubjob.GithubJobApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(modules = [AppModule::class, AndroidSupportInjectionModule::class])
@Singleton
interface AppComponent : AndroidInjector<GithubJobApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<GithubJobApplication>()
}
