package com.nope.githubjob.di

import com.nope.githubjob.misc.getViewModel
import com.nope.githubjob.repository.PositionsRepository
import com.nope.githubjob.ui.search.PositionsAdapter
import com.nope.githubjob.ui.search.SearchFragment
import com.nope.githubjob.ui.search.SearchViewModel
import com.nope.githubjob.ui.search.SearchVm
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
object SearchScreenModule {

    @JvmStatic
    @Provides
    @FragmentScope
    fun adapter(viewModel: SearchVm) = PositionsAdapter(viewModel::onPositionItemClick)

    @JvmStatic
    @Provides
    @FragmentScope
    fun firstScreenVm(
        repository: PositionsRepository,
        router: Router,
        fragment: SearchFragment
    ): SearchVm = fragment.getViewModel { SearchViewModel(repository, router) }
}