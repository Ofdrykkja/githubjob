package com.nope.githubjob.di

import com.nope.githubjob.ui.common.MainNavigator
import com.nope.githubjob.ui.details.DetailsFragment
import com.nope.githubjob.ui.search.SearchFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.terrakok.cicerone.Navigator

@Module
interface MainActivityModule {

    @ActivityScope
    @Binds
    fun navigator(navigator: MainNavigator): Navigator

    @FragmentScope
    @ContributesAndroidInjector(modules = [SearchScreenModule::class])
    fun searchFragment(): SearchFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [DetailsScreenModule::class])
    fun detailsFragment(): DetailsFragment
}
