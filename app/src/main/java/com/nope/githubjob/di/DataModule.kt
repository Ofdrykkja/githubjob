package com.nope.githubjob.di

import com.nope.githubjob.BuildConfig
import com.nope.githubjob.api.PositionsService
import com.nope.githubjob.repository.NetworkPositionsRepository
import com.nope.githubjob.repository.PositionsRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
abstract class DataModule {

    @Binds
    @Singleton
    abstract fun repository(repository: NetworkPositionsRepository): PositionsRepository

    @Module
    companion object {

        @JvmStatic
        @Provides
        @Singleton
        fun dataService(): PositionsService {
            return Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(PositionsService::class.java)
        }
    }
}
