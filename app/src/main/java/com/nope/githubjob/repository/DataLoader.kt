package com.nope.githubjob.repository

import android.arch.lifecycle.LiveData

class DataLoader<T>(val loadingState: LiveData<LoadingState>, val data: T, val isEmpty: LiveData<Boolean>)