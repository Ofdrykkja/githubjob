package com.nope.githubjob.repository

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PageKeyedDataSource
import com.nope.githubjob.api.PositionsService
import com.nope.githubjob.misc.toPreview
import com.nope.githubjob.model.local.PositionPreview
import com.nope.githubjob.model.remote.Position
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by IntelliJ IDEA.
 * Author: aleksander.kiryuhin@e-legion.com
 * Date: 16.08.18
 */
class SearchDataSource(
    private val service: PositionsService,
    private val query: String
) : PageKeyedDataSource<Int, PositionPreview>() {

    val networkState = MutableLiveData<LoadingState>()

    val isEmpty = MutableLiveData<Boolean>()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, PositionPreview>) {
        isEmpty.postValue(true)
        networkState.postValue(LoadingState.LOADING)
        service.getPositions(query, 0).enqueue(object : Callback<List<Position>> {

            override fun onFailure(call: Call<List<Position>>?, t: Throwable?) {
                isEmpty.postValue(true)
                processFailure(t)
            }

            override fun onResponse(call: Call<List<Position>>?, response: Response<List<Position>>?) {
                val responseBody = response?.body()
                if (responseBody != null) {
                    val result = responseBody.toPreview()
                    callback.onResult(result, 0, result.size, 0, 1)
                    isEmpty.postValue(result.isEmpty())
                    networkState.postValue(LoadingState.SUCCESS)
                } else {
                    isEmpty.postValue(true)
                    networkState.postValue(LoadingState.ERROR)
                }
            }
        })
    }

    private fun processFailure(t: Throwable?) {
        networkState.postValue(LoadingState.ERROR)
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, PositionPreview>) {
        service.getPositions(query, params.key).enqueue(object : Callback<List<Position>> {

            override fun onFailure(call: Call<List<Position>>?, t: Throwable?) {
                processFailure(t)
            }

            override fun onResponse(call: Call<List<Position>>?, response: Response<List<Position>>?) {
                val responseBody = response?.body()
                if (responseBody != null) {
                    val result = responseBody.toPreview()
                    callback.onResult(result, params.key + 1)
                    networkState.postValue(LoadingState.SUCCESS)
                } else {
                    networkState.postValue(LoadingState.ERROR)
                }
            }
        })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, PositionPreview>) {
    }
}