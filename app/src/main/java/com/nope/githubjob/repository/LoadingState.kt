package com.nope.githubjob.repository

enum class LoadingState {
    LOADING, SUCCESS, ERROR
}