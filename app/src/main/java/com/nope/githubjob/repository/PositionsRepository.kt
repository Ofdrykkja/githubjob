package com.nope.githubjob.repository

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList
import com.nope.githubjob.model.local.PositionDescription
import com.nope.githubjob.model.local.PositionPreview

interface PositionsRepository {

    fun getPositions(query: String?): DataLoader<LiveData<PagedList<PositionPreview>>>

    fun getPosition(id: String): DataLoader<LiveData<PositionDescription>>
}