package com.nope.githubjob.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.nope.githubjob.api.PositionsService
import com.nope.githubjob.misc.toDescription
import com.nope.githubjob.model.local.PositionDescription
import com.nope.githubjob.model.local.PositionPreview
import com.nope.githubjob.model.remote.Position
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkPositionsRepository @Inject constructor(
    private val config: PagedList.Config,
    private val positionsService: PositionsService
) : PositionsRepository {

    override fun getPositions(query: String?): DataLoader<LiveData<PagedList<PositionPreview>>> {
        val dataSourceFactory = SearchDataSourceFactory(positionsService, query)
        return DataLoader(
            Transformations.switchMap(dataSourceFactory.sourceLiveData) { it.networkState },
            LivePagedListBuilder(dataSourceFactory, config).build(),
            Transformations.switchMap(dataSourceFactory.sourceLiveData) { it.isEmpty }
        )
    }

    override fun getPosition(id: String): DataLoader<LiveData<PositionDescription>> {
        val data = MutableLiveData<PositionDescription>()
        val loadingState = MutableLiveData<LoadingState>()
        val isEmpty = MutableLiveData<Boolean>()
        isEmpty.value = true
        loadingState.value = LoadingState.LOADING
        positionsService.getPosition(id).enqueue(object : Callback<Position> {

            override fun onFailure(call: Call<Position>?, t: Throwable?) {
                processFailure()
            }

            override fun onResponse(call: Call<Position>?, response: Response<Position>?) {
                val responseBody = response?.body()
                if (responseBody != null) {
                    data.value = responseBody.toDescription()
                    loadingState.value = LoadingState.SUCCESS
                    isEmpty.value = false
                } else {
                    processFailure()
                }
            }

            private fun processFailure() {
                data.value = null
                loadingState.value = LoadingState.ERROR
                isEmpty.value = true
            }
        })
        return DataLoader(loadingState, data, isEmpty)
    }
}