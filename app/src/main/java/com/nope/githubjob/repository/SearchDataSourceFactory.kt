package com.nope.githubjob.repository

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.nope.githubjob.api.PositionsService
import com.nope.githubjob.model.local.PositionPreview

class SearchDataSourceFactory(
    private val positionsService: PositionsService,
    private val query: String?
) : DataSource.Factory<Int, PositionPreview>() {

    val sourceLiveData = MutableLiveData<SearchDataSource>()

    override fun create(): DataSource<Int, PositionPreview> {
        val dataSource = SearchDataSource(positionsService, query ?: "")
        sourceLiveData.postValue(dataSource)
        return dataSource
    }
}