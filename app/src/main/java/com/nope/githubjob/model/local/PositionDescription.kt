package com.nope.githubjob.model.local

data class PositionDescription(val description: String)