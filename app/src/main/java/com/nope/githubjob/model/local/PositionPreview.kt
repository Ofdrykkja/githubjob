package com.nope.githubjob.model.local

data class PositionPreview(val id: String, val title: String, val city: String, val logoUrl: String?)